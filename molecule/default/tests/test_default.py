import os
import pytest

import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_hosts_file(host):
    f = host.file('/etc/hosts')

    assert f.exists
    assert f.user == 'root'
    assert f.group == 'root'


@pytest.mark.parametrize('pkg', [
    'secure-delete',
    'curl',
    'socat',
    'git',
    'software-properties-common',
    'dirmngr',
    'ufw',
    'monit',
    'fail2ban'
])
def test_pkg(host, pkg):
    packages = host.packages(pkg)

    assert packages.are_installed


@pytest.mark.parametrize('daemon_pkg', [
    'ufw',
    'monit',
    'fail2ban'
])
def test_pkg_running_and_enabled(host, daemon_pkg):
    packages = host.packages(daemon_pkg)

    assert packages.is_running
    assert packages.is_enabled


def test_fail_2_ban_file(host):
    f = host.file('/var/log/auth.log')

    assert f.exists
    assert f.mode == 0o755
